# Elasticsearch & Kibana Docker Installation

This repository contains a pre-configured Docker Compose file along with the necessary configuration files to create up a single-node Elasticsearch/Kibana
instance with only a few commands. The following guide was carried out on an Ubuntu 23.10 server, but should also work for other Debian derivatives like Kali Purple.

## Install Docker

First, add the official Docker GPG key and add the Docker repository to your APT sources::

```shell
sudo wget https://download.docker.com/linux/ubuntu/gpg -O /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Then update your package lists and install Docker and Docker Compose:

```shell
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin
```

Add your user to the `docker` group to avoid needing to use `sudo` with every Docker command:

```shell
sudo usermod -aG docker $USER
```

You will need to **log out and back in** before running Docker commands as a non-root user.

## Ensure Host Timezone is Correct

To ensure that the self-signed certificates generated in the container have the correct timestamps, make sure your host machine's timezone is set correctly:

```shell
sudo dpkg-reconfigure tzdata
```

Follow the prompts to select your geographic area and timezone.

## Clone Repository and Create Directories

Install Git if it's not already installed, and clone the Elastic Docker repository:

```shell
sudo apt install git
git clone https://gitlab.com/mblows/elastic-docker ~/elastic && cd ~/elastic
```

Edit the `.env` file for your specific instance. The important values are:

`STACK_VERSION`: The current version of Elasticsearch.

`ELASTIC_PASSWORD`: The initial password for the `elastic` user.

`KIBANA_PASSWORD`: A secure password to allow Kibana to connect to Elastic.

`SERVER_FQDN`: The fully qualified domain name of your Elastic server.

```shell
# Password for the 'elastic' user (at least 6 characters)
ELASTIC_PASSWORD=elastic_password

# Password for the 'kibana_system' user (at least 6 characters)
KIBANA_PASSWORD=kibana_password

# Version of Elastic products
STACK_VERSION=8.13.2

# Port to expose Elasticsearch HTTP API to the host
ES_PORT=9200

# Port to expose Kibana to the host
KIBANA_PORT=5601

# Increase or decrease based on the available host memory (in bytes)
MEM_LIMIT=8589934592

# Set the JVM heap size to 50% of MEM_LIMIT
ES_JAVA_OPTS="-Xms4g -Xmx4g"

# Set the server FQDN for the certificates
SERVER_FQDN=elastic-siem.elastic.siem

# Set the server public base URL
SERVER_PUBLICBASEURL="https://${SERVER_FQDN}"
```

Next, create the required data directories and set their permissions:

```shell
mkdir elastic_data certs secrets
sudo chown -R 1000:0 elastic_data certs secrets
```

Make `setup.sh` executable:

```shell
chmod +x setup.sh
```

## Start Elasticsearch and Kibana

Use Docker Compose to start the Elasticsearch and Kibana containers:

```shell
docker compose up -d
```

This will start the containers in detached mode. You can view the logs with `docker-compose logs -f`.

It may take a minute or two for the services to fully start up. You can check their status with:

```shell
docker compose ps
```

The containers are set to restart automatically unless they are stopped, which you can do with `docker compose down` in the same directory as the `docker-compose.yml` file.

Once both services are listed as "healthy" (the `elastic-setup` container should have exited with a status of `0`), you can access them at:

- Elasticsearch: `https://SERVER_FQDN:9200`
- Kibana: `https://SERVER_FQDN:5601`

Use the `elastic` username and the password you set in `ELASTIC_PASSWORD` to log into Kibana.

## Trust the Elastic Self-Signed Certificate

This step is necessary on the host machine before adding the Fleet server.

Copy the generated CA certificate to the trusted certificates directory:

```shell
sudo cp ~/elastic/certs/elastic_ca.crt /usr/local/share/ca-certificates/
```

Update the trusted certificate stores:

```shell
sudo update-ca-certificates
```

Your Elasticsearch and Kibana instance should now be up and running. All files are stored in the `~/elastic` directory, with the elastic data in `elastic_data` and generated certificates in `certs`.

If you wish to back up your installation, just copy the entire `~/elastic` directory to another machine and bring it up with `docker compose up -d` (assuming Docker is installed).

