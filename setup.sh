#!/bin/bash

source .env

SERVER_HOSTNAME=${SERVER_FQDN%%.*}

if [ "${ELASTIC_PASSWORD}" == "" ]; then
    echo "Set the ELASTIC_PASSWORD environment variable in the .env file"
    exit 1
elif [ "${KIBANA_PASSWORD}" == "" ]; then
    echo "Set the KIBANA_PASSWORD environment variable in the .env file"
    exit 1
elif [ "${SERVER_FQDN}" == "" ]; then
    echo "Set the SERVER_FQDN environment variable in the .env file"
    exit 1
fi

# Create the certificates
if [ ! -f config/certs/elastic_ca.crt ]; then
    echo "Creating CA"
    bin/elasticsearch-certutil ca --silent --pem -out config/certs/elastic_ca.zip
    unzip config/certs/elastic_ca.zip -d config/certs
fi
if [ ! -f config/certs/elastic.crt ]; then
    echo "Creating Elastic cert"
    bin/elasticsearch-certutil cert --silent --pem --name elastic --dns localhost,elastic,${SERVER_FQDN},${SERVER_HOSTNAME} --out config/certs/elastic.zip --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key
    unzip config/certs/elastic.zip -d config/certs
fi
if [ ! -f config/certs/kibana.crt ]; then
    echo "Creating Kibana cert"
    bin/elasticsearch-certutil cert --silent --pem --name kibana --dns localhost,kibana,${SERVER_FQDN},${SERVER_HOSTNAME} --out config/certs/kibana.zip --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key
    unzip config/certs/kibana.zip -d config/certs
fi

# Move certificates into the same directory and rename them
echo "Moving and renaming certificates"
mv config/certs/ca/ca.crt config/certs/elastic_ca.crt
mv config/certs/ca/ca.key config/certs/elastic_ca.key
mv config/certs/elastic/elastic.crt config/certs/elastic.crt
mv config/certs/elastic/elastic.key config/certs/elastic.key
mv config/certs/kibana/kibana.crt config/certs/kibana.crt
mv config/certs/kibana/kibana.key config/certs/kibana.key
rm -r config/certs/ca config/certs/elastic config/certs/kibana config/certs/*.zip

# Create the encryption keys for Kibana
echo "Creating encryption keys"
if [ ! -f /usr/share/elasticsearch/config/secrets/xpack_security_encryption_key ]; then
    openssl rand -base64 32 | tr -d '\n' >/usr/share/elasticsearch/config/secrets/xpack_security_encryption_key
fi

if [ ! -f /usr/share/elasticsearch/config/secrets/xpack_reporting_encryption_key ]; then
    openssl rand -base64 32 | tr -d '\n' >/usr/share/elasticsearch/config/secrets/xpack_reporting_encryption_key
fi

if [ ! -f /usr/share/elasticsearch/config/secrets/xpack_encryptedsavedobjects_encryption_key ]; then
    openssl rand -base64 32 | tr -d '\n' >/usr/share/elasticsearch/config/secrets/xpack_encryptedsavedobjects_encryption_key
fi

# Set the file permissions to more secure values
echo "Setting file permissions"
chown -R root:root config/certs
find config/certs -type d -exec chmod 750 \{\} \;
find config/certs -type f -exec chmod 640 \{\} \;

chown -R root:root config/secrets
find config/secrets -type d -exec chmod 750 \{\} \;
find config/secrets -type f -exec chmod 640 \{\} \;

echo "Waiting for Elasticsearch availability"
until curl -s --cacert config/certs/elastic_ca.crt https://elastic:9200 | grep -q "missing authentication credentials"; do sleep 10; done

echo "Setting kibana_system password"
until curl -s -X POST --cacert config/certs/elastic_ca.crt -u "elastic:${ELASTIC_PASSWORD}" -H "Content-Type: application/json" https://elastic:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 10; done
echo "All done!"
